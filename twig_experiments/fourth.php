<?php

$input = file_get_contents("http://php.net/feed.atom");
$data = new SimpleXMLElement($input);

// Convert to raw array (twix needs this anyway)
$json_string = json_encode($data);
$data1 = json_decode($json_string, TRUE);


$id = 0;
foreach ($data->entry as $item) {
  $xml = $item->content->div->asXML();  // Turn content into plain HTML
  $data1['entry'][$id]['html'] = $xml;  // Add content to raw array
  $id++;
}

require_once '../twig/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

echo $twig->render('fourth.twig', $data1);
