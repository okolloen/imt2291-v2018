let downloadFolder = '';
let resultDOMNode = null;
const me = 'Øivind Kolloen';    // Used to filter out my own commits

window.addEventListener('load', e=>{          // When document is loaded
  if (localStorage.getItem('uname')!=null&&localStorage.getItem('uname')!='') {
    document.getElementById('username').value = localStorage.getItem('uname');
    document.getElementById('repo').value = localStorage.getItem('repo');
    document.getElementById('downloadFolder').value = localStorage.getItem('dfolder');
  }
  resultDOMNode = document.querySelector('#result');
  downloadFolder = document.querySelector('#downloadFolder').value;
  document.querySelector('button[name="showRepos"]').addEventListener('click', e=>{
    var data = new FormData(e.target.form);
    localStorage.setItem('uname', e.target.form.bbUname.value);
    localStorage.setItem('repo', e.target.form.repo.value);
    localStorage.setItem('dfolder', downloadFolder);
    fetch('api/getForks.php', {               // Fetch all forks
      body: data,
      method: 'POST'
    })
    .then (res=>res.json())
    .then (data=>{
      data.forEach(item=>{
        resultDOMNode.innerHTML += item+'<br>';
      })
    });
  });
  document.querySelector('button').addEventListener('click', e=>{ // Add on click handler to button
    downloadFolder = document.querySelector('#downloadFolder').value;
    var data = new FormData(e.target.form);   // Used to POST form data width fetch
    localStorage.setItem('uname', e.target.form.bbUname.value);
    localStorage.setItem('repo', e.target.form.repo.value);
    localStorage.setItem('dfolder', downloadFolder);
    console.log (e);
    resultDOMNode.innerHTML += `Henter oversikt over forks av ${e.target.form.repo.value}.<br>`;
    fetch('api/getForks.php', {               // Fetch all forks
      body: data,
      method: 'POST'
    })
    .then (res=>res.json())                   // decode JSON data
    .then (repos=> downloadRepos(repos))      // Download all repositories
    .then (repos=> repositoriesStatistics(repos))   // Generate statistics on all repositories
    .then (repos=> downloadWikis(repos))      // Download Wiki for all repositories
    .then (res => {                           // Dump list of repositories handled to console
      console.log (res);
    })
    .catch (err => {                          // If error during process, show error in console
      console.log (err);
    })
  })
});

/**
 * Get an array with all repositories, returns a Promise that resolves with the
 * same list of repositories when the repositories have been downloaded.
 *
 * @param  Array repos list of repositories to download
 * @return Promise resolves with an array of repositories that have been downloaded
 */
function downloadRepos(repos) {
  resultDOMNode.innerHTML += `${repos.length} forks funnet, starter nedlasting.<br>`;
  return new Promise((resolve, reject) =>{
    const downloads = [];
    repos.forEach(repo=>{
      downloads.push(downloadRepo(repo));
    })
    Promise.all(downloads)
    .then (res=>{
      resolve(repos);
    })
    .catch (err=>{
      reject(err);
    })
  })
}

/**
 * Used by the Promise generated in downloadRepos to download a single repository.
 * Excepts a String with the name of the repository to download, gets the username
 * and password and downloadFolder from the form the user filled in before initiating
 * the download.
 *
 * Uses api/downloadRepo.php to download the repository.
 *
 * Adds messages to the resultDOMNode when download starts and finishes.
 *
 * @param  String repo name of the repository to download
 * @return Promise resolves when the repository has been downloaded.
 */
function downloadRepo (repo) {
  resultDOMNode.innerHTML += `Starter nedlasting av ${repo}<br>`;
  return new Promise ((resolve, reject) => {
    const data = new FormData(document.querySelector('form'));
    data.append ('repo', repo);
    data.append ('downloadFolder', downloadFolder);
    fetch('api/downloadRepo.php', {
      body: data,
      method: 'POST'
    })
    .then (res=>{
      resultDOMNode.innerHTML += `ferdig med nedlasting av ${repo}<br>`;
      resolve(res);
    })
    .catch (err=>{
      reject(err);
    })
  })
}

/**
 * Expects an array of repository names to generate user activity statistics for.
 *
 * @param  Array repos with a list of repository names
 * @return Promise will resolve with an array of repository names that staticts have been
 * generated for when all repositories have been processed.
 */
function repositoriesStatistics(repos) {
  resultDOMNode.innerHTML += `=================<br>Finner statistikk for ${repos.length} repositorier.<br>`;
  return new Promise((resolve, reject) =>{
    const stats = [];
    repos.forEach(repo=>{
      stats.push(repositoryStatistics(repo));
    })
    Promise.all(stats)    // Wait for statistics to be generated for all repositories
    .then (res=>{
      resolve(repos);
    })
    .catch (err=>{
      reject(err);
    })
  })
}

/**
 * Will add user activity statistics for this repo to the resultDOMNode.
 * User names and commits, merges, lines added and removed will be added for all
 * users that have had activity on this repository.
 *
 * @param  String repo name of the repository
 * @return Promise resolves when the statictics have been generated
 */
function repositoryStatistics (repo) {
  resultDOMNode.innerHTML += `Finner statistikk for ${repo}<br>`;
  return new Promise ((resolve, reject) => {
    fetch(`api/gitStats.php?repo=${encodeURIComponent(repo)}&baseFolder=${downloadFolder}`)
    .then (res=> res.json())
    .then (data=> {
      resultDOMNode.innerHTML += `------------<br>${repo}:<br>`;
      let html = '<ul>';
      data.forEach(person=>{
        if (person.user!=me) {
          html += `<li>${person.user}<ul>`;
          html += `<li><span>Commits</span><span class="value">${person.commits}</span></li>`;
          html += `<li><span>Merges</span><span class="value">${person.merges}</span></li>`;
          html += `<li><span>Lines added</span><span class="value">${person.linesAdded}</span></li>`;
          html += `<li><span>Lines deleted</span><span class="value">${person.linesDeleted}</span></li>`;
          html += `</ul></li>`;
        }
      })
      html += '</ul>';
      resultDOMNode.innerHTML += html;
      resolve(html);
    })
    .catch (err=>{
      reject(err);
    })
  })
}

/**
 * Get an array with all repositories, returns a Promise that resolves with the
 * same list of repositories when the wiki have been downloaded for all repositories.
 *
 * @param  Array repos list of repositories to download the wiki's for
 * @return Promise resolves with an array of repositories the wiki's have been downloaded for
 */
function downloadWikis(repos) {
  resultDOMNode.innerHTML += `Laster ned ${repos.length} Wiki'er.<br>`;
  return new Promise((resolve, reject) =>{
    const downloads = [];
    repos.forEach(repo=>{
      downloads.push(downloadWiki(repo));
    })
    Promise.all(downloads)
    .then (res=>{
      resolve(repos);
    })
    .catch (err=>{
      reject(err);
    })
  })
}

/**
 * Used by the Promise generated in downloadWikis to download a single Wiki.
 * Excepts a String with the name of the repository to download the Wiki for, gets the username
 * and password and downloadFolder from the form the user filled in before initiating
 * the download.
 *
 * Uses api/downloadWiki.php to download the wiki.
 *
 * Adds messages to the resultDOMNode when download starts and finishes.
 *
 * @param  String repo name of the repository to download the wiki for
 * @return Promise resolves when the wiki has been downloaded.
 */
function downloadWiki (repo) {
  resultDOMNode.innerHTML += `Laster ned Wiki for ${repo}<br>`;
  return new Promise ((resolve, reject) => {
    const data = new FormData(document.querySelector('form'));
    data.append ('repo', repo);
    data.append ('downloadFolder', downloadFolder);
    fetch('api/downloadWiki.php', {
      body: data,
      method: 'POST'
    })
    .then (res=>{
      resultDOMNode.innerHTML += `Wiki for ${repo} er lastet ned<br>`;
      resolve(res);
    })
    .catch (err=>{
      reject(err);
    })
  })
}
