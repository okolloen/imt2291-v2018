<?php
use PHPUnit\Framework\TestCase;

require_once 'vendor/autoload.php';
require_once './src/classes/DB.php';

final class DBTest extends TestCase {
  public function testCanConnectToDB() {
    $this->assertInstanceOf(
      PDO::class,
      DB::getDBConnection()
    );
  }
}
