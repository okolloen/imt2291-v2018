<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

/**
 * Class contains all functional tests for this web application.
 *
 */
class FunctionalTests extends TestCase {
  /* Change this to suite your server setup */
  protected $baseUrl = "http://localhost/imt2291-v2018/uke_4_forelesning/src/";
  protected $session;
  protected $pageTitles = array ('index.html' => 'Kontaktregister',
                                  'addContact.php' => 'Legg til en kontakt',
                                  'addContact.php?contactAdded' => 'Lagt til kontakt',
                                  'editContact.php' => 'Rediger informasjon om kontakt',
                                  'listContacts.php' => 'Alle kontakter i registeret',
                                  'searchContact.php' => 'Søk etter kontakter i registeret');
  protected $familyName, $givenName;

  /**
   * Initiates the testing session
   */
  protected function setup() {
    $driver = new \Behat\Mink\Driver\GoutteDriver();
    $this->session = new \Behat\Mink\Session($driver);
    $this->session->start();
    $this->familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $this->givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
  }

  /**
   * Simple test to see if we can get the title of the main page.
   */
  public function testCanGetPage() {
    $this->session->visit($this->baseUrl.'index.html');
    $page = $this->session->getPage();

    $this->assertEquals ($this->pageTitles['index.html'],
                         $this->getPageTitle($page), 'Wrong title on page');
  }

  public function testCanAddContact() {
    $this->session->visit($this->baseUrl.'addContact.php');
    $page = $this->session->getPage();

    // Check for correct page name
    $this->assertEquals ($this->pageTitles['addContact.php'],
                         $this->getPageTitle($page), 'Wrong title on page');

    // Add a contact and get the id of the contact that was added
    $id = $this->addContact($page);

    // Delete contact with given ID
    $this->session->visit($this->baseUrl.'deleteContact.php?id='.$id);
    $page = $this->session->getPage();

    // Check that the contact was deleted
    $this->assertTrue($page->find('css', 'h1')!=null, 'Missing title on page');
    $this->assertEquals('Kontakten ble slettet', $page->find('css', 'h1')->getText(), 'Contact could not be deleted');
  }

  public function testListContacts() {
    $this->session->visit($this->baseUrl.'listContacts.php');
    $page = $this->session->getPage();

    // Check for correct page name
    $this->assertEquals ($this->pageTitles['listContacts.php'],
                         $this->getPageTitle($page), 'Wrong title on page');

    $this->assertInstanceOf(
                           NodeElement::Class,
                           $page->find('css', 'table th:nth-child(2)'),
                           'Missing TH element, should have table with list of contacts'
                         );
    $this->assertEquals('Fornavn', $page->find('css', 'table th:nth-child(2)')->getText(),
                        'Wrong text in header field');
  }

  /**
   * Gets the head/title value for the given page.
   */
  private function getPageTitle($page) {
    $title = null;
    $titleEl = $page->find('xpath', 'head/title');

    // Page has a title element
    if ($titleEl) {
        $title = $titleEl->getText();
    }
    return $title;
  }

  private function addContact ($page) {
     // Fills in all fields of the addContact form
    $this->fillInAddContactForm($page);

    // Submit the form and get the resulting page
    $form = $page->find('css', 'form[action="addContact.php"]');
    $form->submit();
    $page = $this->session->getPage();

    // Check for correct page name (form should not be submitted)
    $this->assertEquals ($this->pageTitles['addContact.php?contactAdded'],
                         $this->getPageTitle($page), 'Contact not added');

    // Get the id of the contact that was added
    $h1 = $page->find('css', 'h1');
    $this->assertTrue ($h1->hasAttribute('data-id'), 'Missing data-id attribute from H1 tag');
    $id = $h1->getAttribute('data-id'); // This is the id of the contact thas was added
    return $id;
  }

  private function fillInAddContactForm($page) {
    $this->checkForFieldAndInsertValue($page, 'familyName', $this->familyName,
                    'Input field for "familyName" not found.');

    $this->checkForFieldAndInsertValue($page, 'givenName', $this->givenName,
                    'Input field for "givenName" not found.');

    $this->checkForFieldAndInsertValue($page, 'phone', '12345678',
                    'Input field for "phone" not found.');

    $this->checkForFieldAndInsertValue($page, 'email', 'autoTest@autotest.test',
                    'Input field for "email" not found.');
  }

  private function checkForFieldAndInsertValue($page, $fieldId, $value, $message) {
    // Check that field with given id exists on page
    $this->assertInstanceOf(
                           NodeElement::Class,
                           $page->find('named', array('field', $fieldId)),
                           $message
                         );
    // Set value of field
    $page->find('named', array('field', $fieldId))->setValue($value);
  }
}
