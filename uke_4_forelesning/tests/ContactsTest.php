<?php
use PHPUnit\Framework\TestCase;

require_once 'vendor/autoload.php';
require_once './src/classes/DB.php';
require_once './src/classes/Contacts.php';

/**
 * Tests for functionality (Create, Delete, Get, Search, Edit, List) of
 * Contacts class.
 */
final class DBTest extends TestCase {
  private $familyName, $givenName, $contact;
  private $contacts;

  // Set up test environment, create a contact with random family and given name.
  // Also set up an object of the Contacts class.
  protected function setup() {
    $db = DB::getDBConnection();
    $this->familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $this->givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
    $this->contact['givenName'] = $this->givenName;
    $this->contact['familyName'] = $this->familyName;
    $this->contact['email'] = $this->givenName.'@'.$this->familyName.'.test';
    $this->contact['phone'] = '12345678';

    $this->contacts = new Contacts($db);
  }

  /**
   * Checks if a contact can be created and deleted.
   * Checks that the id of the created contact is > 0 and that a contacts
   * with this id can be deleted.
   */
  public function testCanCreateAndDeleteContact() {
    $result = $this->contacts->addContact($this->contact);
    $this->assertEquals('OK', $result['status'], 'Failed to create contact');
    $contactID = $result['id'];
    $this->assertTrue($contactID>0, 'Contact ID should be > 0');

    // Delete the contact
    $result = $this->contacts->deleteContact($contactID);
    $this->assertEquals('OK', $result['status'], 'Unable to delete contact');
  }

  /**
   * Create a new contact and check that a contact with the id of then
   * created contact can be retrieved and that the name of the retrieved
   * contact matches the added contact.
   */
  public function testCanGetContact() {
    $result = $this->contacts->addContact($this->contact);
    $contactID = $result['id'];

    // Get contact
    $result = $this->contacts->getContact($contactID);
    $this->assertEquals('OK', $result['status'], 'Unable to find contact');
    $this->assertEquals($this->givenName, $result['contact']['givenName'], 'Given name did not match');

    // Delete the contact
    $result = $this->contacts->deleteContact($contactID);
  }

  /**
   * Create a contact and checks that this contact can be found by given name,
   * family name and a combination of both.
   * Remove the contact when complete.
   */
  public function testCanSearchForContact() {
    $result = $this->contacts->addContact($this->contact);
    $contactID = $result['id'];

    // Search for contact (on given name)
    $result = $this->contacts->searchContacts($this->givenName);
    $this->assertEquals('OK', $result['status'], 'Unable to find contact');
    $this->assertEquals($this->familyName, $result['contacts'][0]['familyName'],
                        'Family name did not match');
    // Search for contact (on family name)
    $result = $this->contacts->searchContacts($this->familyName);
    $this->assertEquals('OK', $result['status'], 'Unable to find contact');
    $this->assertEquals($this->givenName, $result['contacts'][0]['givenName'],
                        'Givem name did not match');
    // Search for contact (on given name + space + family name)
    $result = $this->contacts->searchContacts($this->givenName.' '.$this->familyName);
    $this->assertEquals('OK', $result['status'], 'Unable to find contact');

    // Delete the contact
    $result = $this->contacts->deleteContact($contactID);
  }

  /**
   * Add a contact, then update (change), the added contact. Checks that the
   * update is registered. Delete the contact when done.
   */
  public function testCanUpdateContact() {
    $result = $this->contacts->addContact($this->contact);
    $contactID = $result['id'];

    $updatedContactInfo = $this->contact;
    $updatedContactInfo['id'] = $contactID;
    $updatedContactInfo['givenName'] = 'test';

    // Update contact
    $result = $this->contacts->updateContact($updatedContactInfo);
    $this->assertEquals('OK', $result['status']);
    // Get contact
    $result = $this->contacts->getContact($contactID);
    $this->assertEquals('OK', $result['status'], 'Unable to find contact');
    $this->assertEquals('test', $result['contact']['givenName'], 'Given name did not match/was not updated');

    // Delete the contact
    $result = $this->contacts->deleteContact($contactID);
  }

  /**
   * Add a contact where the family name starts with "##", since contacts should
   * be sorted by family name when listed this contact should be the first
   * contact in the returned list. Checks that this is correct.
   * Delete the contact when done.
   */
  public function testListContacts() {
    $easySortContact = $this->contact;
    $easySortContact['familyName'] = '##'.$this->familyName;  // Assume no contacts will be sorted over this one
    $result = $this->contacts->addContact($easySortContact);
    $contactID = $result['id'];

    // List contacts
    $result = $this->contacts->listContacts();
    $this->assertEquals('OK', $result['status'], 'Unable to get list of contacts');
    $this->assertEquals($this->givenName,
                        $result['contacts'][0]['givenName'],
                        'Given name did not match');

    // Delete the contact
    $result = $this->contacts->deleteContact($contactID);
  }
}
