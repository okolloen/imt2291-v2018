<?php

require_once '../../twig/vendor/autoload.php';
require_once "classes/Contacts.php";
require_once "classes/DB.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['givenName'])) {  // Changed from "addContact" to work with mink/GoutteDriver
  echo $twig->render('addContactForm.html', array());
} else {
  $data['givenName'] = $_POST['givenName'];
  $data['familyName'] = $_POST['familyName'];
  $data['phone'] = $_POST['phone'];
  $data['email'] = $_POST['email'];

  $db = DB::getDBConnection();
  /*
  if ($db==null) {
    // show error page and exit
  } */

  $contacts = new Contacts($db);
  $res = $contacts->addContact ($data);
  $res['data'] = $data;

  echo $twig->render('contactAdded.html', $res);
}
