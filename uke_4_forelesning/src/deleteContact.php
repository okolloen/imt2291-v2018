<?php

require_once '../../twig/vendor/autoload.php';
require_once "classes/Contacts.php";
require_once "classes/DB.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (isset($_GET['id'])) {
  $db = DB::getDBConnection();
  /*
  if ($db==null) {
    // show error page and exit
  } */

  $contacts = new Contacts($db);
  $res = $contacts->deleteContact ($_GET['id']);
  $res['data'] = $data;

  echo $twig->render('contactDeleted.html', $res);
}
