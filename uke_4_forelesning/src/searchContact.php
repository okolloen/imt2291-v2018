<?php

require_once '../../twig/vendor/autoload.php';
require_once 'classes/Contacts.php';
require_once 'classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

$data = [];
if  (isset($_GET['search'])) {
  $contacts = new Contacts(DB::getDBConnection());
  $data = $contacts->searchContacts ($_GET['search']);
}

echo $twig->render('searchContacts.html', $data);
