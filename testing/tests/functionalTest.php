<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

/**
 * Class contains all functional tests for this web application.
 *
 */
class FunctionalTests extends TestCase {
  /* Change this to suite your server setup */
  protected $baseUrl = "http://localhost/imt2291-v2018/testing/src/index.php";
  protected $session;
  protected $pageTitles = array ('mainPage' => 'Postal code/area search system');

  /**
   * Initiates the testing session
   */
  protected function setup() {
    $driver = new \Behat\Mink\Driver\GoutteDriver();
    $this->session = new \Behat\Mink\Session($driver);
    $this->session->start();
  }

  /**
   * Gets the head/title value for the given page.
   */
  private function getPageTitle($page) {
    $title = null;
    $titleEl = $page->find('xpath', 'head/title');

    // Page has a title element
    if ($titleEl) {
        $title = $titleEl->getText();
    }
    return $title;
  }

  /**
   * Simple test to see if we can get the title of the page.
   */
  public function testCanGetPage() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $this->assertEquals ($this->pageTitles['mainPage'],
                         $this->getPageTitle($page), 'Wrong title on page');
  }

  /**
   * Test searching for postal code.
   */
  public function testCanSearchForPostalCode() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $form = $page->find('xpath', 'body/form');  // First form under body

    if ($form!=null) {
      $input = $form->find('css', 'input[id="postal"]');
      if ($input!=null) {
        $input->setValue('2850');
      } else {
        $this->assertTrue(false, 'Input field not found');
      }
    } else {
      $this->assertTrue(false, 'Form not found');
    }
    $form->submit();

    $page = $this->session->getPage();
    $result = $page->find('xpath', '//div[@id="result"]');  // Any div with given id
    if ($result!=null) {
      $this->assertEquals('2850 Lena, Østre Toten', $result->getText());
    } else {
      $this->assertTrue(false, 'No result data found');
    }
  }

  /**
   * Test searching for postal code.
   */
  public function testCanSearchForPlaceName() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $form = $page->find('xpath', 'body/form');  // First form under body

    if ($form!=null) {
      $input = $form->find('css', 'input[id="postal"]');
      if ($input!=null) {
        $input->setValue('Lena');
      } else {
        $this->assertTrue(false, 'Input field not found');
      }
    } else {
      $this->assertTrue(false, 'Form not found');
    }
    $form->submit();

    $page = $this->session->getPage();
    $result = $page->find('xpath', '//div[@id="result"]');  // Any div with given id
    if ($result!=null) {
      $this->assertEquals('2850 Lena, Østre Toten 2851 Lena, Østre Toten',
                          $result->getText());
    } else {
      $this->assertTrue(false, 'No result data found');
    }
  }
}
