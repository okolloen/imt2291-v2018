<?php

require_once(__DIR__ . "/../src/PostalSearch.php");

use PHPUnit\Framework\TestCase;

final class PostalSearchTest extends TestCase {
  public function testPostalCodeSearch() {
    $this->assertEquals('Lena',
                        PostalSearch::postalCodeSearch('2850')[0]->placeName);
  }

  public function testPlaceNameSearch() {
    $postalInfo = PostalSearch::placeNameSearch('Lena');
    $this->assertEquals(2, count($postalInfo));
    $this->assertEquals("2850", $postalInfo[0]->postalCode);
  }
}
