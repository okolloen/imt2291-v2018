$(function(){
  $(".wrapper>div>textarea").prop("disabled", true);
  $(".flexcol textarea").change(function(){
    let lines = this.value.split("\n");
    let css = {};
    for (let i=0; i<lines.length; i++) {
      if (lines[i].includes(":")) {
        let nameVal = lines[i].split(":");
        css[nameVal[0].trim()] = nameVal[1].trim().substring(0,nameVal[1].length-2);
      }
    }
    $(".firstContainer "+this.dataset.selector).css(css);
  });
});
