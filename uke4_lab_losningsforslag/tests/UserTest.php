<?php
use PHPUnit\Framework\TestCase;

require_once 'vendor/autoload.php';
require_once './classes/DB.php';
require_once './classes/User.php';

/**
 * Tests for functionality (Create, Delete, Get, Search, Edit, List) of
 * Contacts class.
 */
final class ContactsTest extends TestCase {
  private $familyName, $givenName, $userData;
  private $pwd = 'mySecretPassword';
  private $user;
  private $db;

  protected function setup() {
    $db = DB::getDBConnection();
    $this->familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $this->givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
    $this->userData['givenName'] = $this->givenName;
    $this->userData['familyName'] = $this->familyName;
    $this->userData['uname'] = $this->givenName.'@'.$this->familyName.'.test';
    $this->userData['pwd'] = $this->pwd;

    $this->user = new User($db);
  }

  /**
   * Checks if it is possible through an object of the User class to add a
   * new user to the user table in the database.
   */
  public function testCanCreateUser() {
    $data = $this->user->addUser($this->userData);
    $this->assertEquals('OK', $data['status'], 'Failed to create new user.');
    $this->assertTrue($data['id']>0, 'ID of new user should be > 0.');

    // Delete user
    $deleted = $this->user->deleteUser($data['id']);
    $this->assertEquals('OK', $deleted['status'], 'User could not be deleted.');
  }

  /**
   * Checks if it possible to log in to a newly created user instance.
   */
  public function testCanDoLogin() {
    $data = $this->user->addUser($this->userData);
    $loginstatus = $this->user->login($this->userData['uname'], $this->pwd);
    $this->assertEquals('OK', $loginstatus['status'], 'Failed to log in.');

    // Delete user
    $deleted = $this->user->deleteUser($data['id']);
  }
}
