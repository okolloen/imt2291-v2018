<?php
session_start();
header('Content-type: application/json');
if (isset($_SESSION['access_token'])) {
  echo json_encode (array('access_token' => $_SESSION['access_token'],
                            'token_type' => $_SESSION['token_type'],
                            'refresh_token' => $_SESSION['refresh_token'],
                            'scope' => $_SESSION['scope']));
} else {
  echo json_encode (array('error'=>'Not logged in.'));
}
