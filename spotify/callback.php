<?php
session_start();
$code = $_GET['code'];

$url = 'https://accounts.spotify.com/api/token';
$data = array('grant_type' => 'authorization_code', 'code' => $code,
              'redirect_uri'=>'http://localhost/imt2291/spotify/callback.php',
              'client_id'=>'41f7402c6ec84a56a3ab7547e98cfc53',
              'client_secret'=>'dfb5f784d91d4375a27accf3878b8e60');

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { /* Handle error */ }

$data = json_decode($result, true);
$_SESSION['access_token'] = $data['access_token'];
$_SESSION['token_type'] = $data['token_type'];
$_SESSION['refresh_token'] = $data['refresh_token'];
$_SESSION['scope'] = $data['scope'];

header("Location: index.html");
