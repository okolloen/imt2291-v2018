<?php
require_once '../../twig/vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

if (!isset($_FILES['fileToUpload'])) {
  echo $twig->render('upload.html', array());
  exit();
}

$db = DB::getDBConnection();

if (is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
  $content = file_get_contents($_FILES['fileToUpload']['tmp_name']);
  $mime = $_FILES['fileToUpload']['type'];
  $name = $_FILES['fileToUpload']['name'];
  $size = $_FILES['fileToUpload']['size'];
  $sql = "INSERT INTO filesInDB (owner, name, mime, size, description, content) VALUES (1, :name, :mime, :size, :description, :content)";
  $sth = $db->prepare ($sql);
  $sth->bindParam(':name', $name);
  $sth->bindParam(':mime', $mime);
  $sth->bindParam(':size', $size);
  $sth->bindParam(':description', $_POST['descr']);
  $sth->bindParam(':content', $content);
  $sth->execute ();
  if ($sth->rowCount()==1) {
    echo $twig->render('uploadSuccess.html', array('fname'=>$name, 'size'=>$size));
  } else {
    echo $twig->render('uploadFailed.html', array('fname'=>$name));
  }
} else {  // Some trickery is going on
  echo $twig->render('badbadbad.html', array());
}
