<?php
require_once '../../twig/vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

if (!isset($_FILES['fileToUpload'])) {
  echo $twig->render('upload.html', array());
  exit();
}

$db = DB::getDBConnection();

if (is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
  $content = file_get_contents($_FILES['fileToUpload']['tmp_name']);
  $height = $_POST['height']*1;
  $width = $_POST['width']*1;
  $scaledContent = scale (imagecreatefromstring($content), $width, $height);
  unset ($content);     // Free up memory from old/unscaled image
  $mime = $_FILES['fileToUpload']['type'];
  $name = $_FILES['fileToUpload']['name'];
  $name = substr($name, 0, strrpos($name, '.')).'.png';
  $size = $_FILES['fileToUpload']['size'];
  $sql = "INSERT INTO filesInDB (owner, name, mime, size, description, content) VALUES (1, :name, :mime, :size, :description, :content)";
  $sth = $db->prepare ($sql);
  $sth->bindValue(':name', $name); // scaled image will be png format
  $sth->bindValue(':mime', 'image/png');
  $sth->bindParam(':size', strlen($scaledContent));
  $sth->bindParam(':description', $_POST['descr']);
  $sth->bindParam(':content', $scaledContent);
  $sth->execute ();
  if ($sth->rowCount()==1) {
    echo $twig->render('uploadSuccess.html', array('fname'=>$name, 'size'=>strlen($scaledContent)));
  } else {
    echo $twig->render('uploadFailed.html', array('fname'=>$name));
  }
} else {  // Some trickery is going on
  echo $twig->render('badbadbad.html', array());
}

function scale ($img, $new_width, $new_height) {
  $old_x = imageSX($img);
  $old_y = imageSY($img);

  if($old_x > $old_y) {                     // Image is landscape mode
    $thumb_w = $new_width;
    $thumb_h = $old_y*($new_height/$old_x);
  } else if($old_x < $old_y) {              // Image is portrait mode
    $thumb_w = $old_x*($new_width/$old_y);
    $thumb_h = $new_height;
  } if($old_x == $old_y) {                  // Image is square
    $thumb_w = $new_width;
    $thumb_h = $new_height;
  }

  if ($thumb_w>$old_x) {                    // Don't scale images up
    $thumb_w = $old_x;
    $thumb_h = $old_y;
  }

  $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
  imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

  ob_start();                         // flush/start buffer
  imagepng($dst_img,NULL,9);          // Write image to buffer
  $scaledImage = ob_get_contents();   // Get contents of buffer
  ob_end_clean();                     // Clear buffer
  return $scaledImage;
}
