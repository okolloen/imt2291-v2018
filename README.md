# Kildekode for IMT2291 våren 2018 #

All kildekode i IMT2291 vårsemesteret 2018 legges i dette repositoriet. Gjør en "git pull" av og til for å få de siste oppdateringer. Dersom noen oppdager feil eller uklarheter i koden så er det fint om dere legger det inn som et Issue, det vil da bli korrigert.

# Organisering #

Koden ligger med et tema pr. katalog, med unntak av kode fra labtimene som ligger i katalogen "Kode fra lab" og så i underkataloger med uke/emne under der igjen.