<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laboppgave 1, uke 3</title>
</head>
<body>
  <?php
    require_once "Pinterest.php";

    foreach (Pinterest::getPins("mathematical riddles fun") as $pin) {
      echo "$pin<br/>\n";
    }
   ?>
</body>
</html>
