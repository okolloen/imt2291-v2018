<?php

require_once '../../twig/vendor/autoload.php';
require_once "classes/Contacts.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

$data = [];
if  (isset($_GET['id'])) {
  $contacts = new Contacts();
  $data = $contacts->getContact ($_GET['id']);
  echo $twig->render('editContact.html', $data);
} else if (isset($_POST['updateContact'])) {
  $contacts = new Contacts();
  $data = $contacts->updateContact($_POST);
  echo $twig->render('contactUpdated.html', $data);
} else {
  echo $twig->render('error.html', array ("message"=>"Ingen kontaktinformasjon anngitt."));
}
